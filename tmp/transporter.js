(function(global){

	function Transporter(host, params, cb) {
		var self = this;
		this.queue = [];
		this.request_id = 0;
		this.callbacks = {};
		this.websocket = new SockJS(host+"/io", params, null, {
			protocols_whitelist : ['websocket','xhr-polling']
		});
		this.websocket.onopen = function(evt) {
			self.flush();
			myconsole.log("transporter:onopen", evt);
		};
		this.websocket.onclose = function(evt) {
			myconsole.log("transporter:onclose", evt);
		};
		this.websocket.onmessage = function(evt) {
			var mes = JSON.parse(evt.data);
			self._onMessage(mes);
			if(self.callbacks[mes.r]) self.callbacks[mes.r](mes);
		};
		this.websocket.onerror = function(evt) {
			myconsole.log("transporter:onerror", evt);
		};
		if(cb) cb();
	}

	Transporter.prototype.onMessage = function(cb) {
		this._onMessage = cb;
	}
	Transporter.prototype.cast = function(msg) {
	}

	Transporter.prototype.call = function(msg, cb) {
	}

	Transporter.prototype.send_operation_request = function(optype, content, cb) {
		this.request_id++;
		this.callbacks[this.request_id] = cb;
		this.send(OperationEncoder.encode_operation(optype, this.request_id, content));
	}
	Transporter.prototype.send = function(msg) {
		if(this.websocket.readyState === SockJS.OPEN) {
			this.websocket.send(JSON.stringify(msg));
		}else{
			this.queue.push(JSON.stringify(msg));
		}
	}

	Transporter.prototype.flush = function() {
		this.send_next();
	}

	Transporter.prototype.send_next = function() {
		if(this.websocket.readyState === SockJS.OPEN) {
			var msg = this.queue.shift();
			if(msg) {
				this.websocket.send(msg);
				this.send_next();
			}
		}
	}

	Transporter.prototype.disconnect = function(){
		this.websocket.close();
	}

	function OperationEncoder() {

	}
	OperationEncoder.encode_operation = function(event, request_id, content) {
		return {"e" : event, "r" : request_id, "c" : content};
	}
	OperationEncoder.decode_result = function(text) {
		var r = JSON.parse(text);
		return {
			request_id : r.r,
			content : r.c
		}
	}
	OperationEncoder.encode_erlang_path = function(path){
		pathArray = [];
		if(path.length > 0){
			path.split("/").forEach(function(str){
				var res = "";
				var upperCaseUtf8Array = escape(str).split("%").join("\\").split("\\u");
				res += upperCaseUtf8Array[0];
				for(var i=1; i<upperCaseUtf8Array.length; i++){
					res += "\\u" + upperCaseUtf8Array[i].substr(0,4).toLowerCase();
					res += upperCaseUtf8Array[i].substr(4);
				}
				pathArray.push(res);
			});
	    	return pathArray.join("/");
		} else {
			return "";
		}
	}
	OperationEncoder.decode_broadcast = function(text) {
		var r = JSON.parse(text);
		return {
			optype : r.o,
			content : r.c
		}
	}

	if(!global.hasOwnProperty("MilkCocoaUtil"))
		global.MilkCocoaUtil = {};
	global.MilkCocoaUtil.Transporter = Transporter;
	global.MilkCocoaUtil.OperationEncoder = OperationEncoder;

}(window));
