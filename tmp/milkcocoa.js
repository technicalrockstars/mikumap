(function(global){
	var myconsole = {
		log : function(v1, v2) {
			if(window.console) console.log(v1, v2);
		},
		error : function(v1, v2) {
			if(window.console) console.error(v1, v2);
		}
	}

	function MilkCocoa(host) {
		var self = this;
		this.listeners = new MilkCocoaUtil.Listeners();
		var app_id = host.match(/([a-z0-9]+)\.mlkcca\.com/)[1];
		this.app_id = app_id;
		this.host = host;
		this.idGenerator = new MilkCocoaUtil.IdGenerator();
		this.transporter = new MilkCocoaUtil.Transporter(host, {client : "MilkCocoa", app_id : app_id});
		this.transporter.onMessage(function(msg) {
			switch(msg.e) {
				case "push":
					self.listeners.fire("push", msg.c.path, {
						id : msg.c.id,
						path : msg.c.path,
						value : JSON.parse(msg.c.params)
					});
					break;
				case "set":
					self.listeners.fire("set", msg.c.path, {
						id : msg.c.id,
						path : msg.c.path,
						value : JSON.parse(msg.c.params)
					});
					break;
				case "remove":
					self.listeners.fire("remove", msg.c.path, msg);
					break;
				case "send":
					self.listeners.fire("send", msg.c.path, {
						path : msg.c.path,
						value : JSON.parse(msg.c.params)
					});
					break;
				case "success":
					console.log("success", msg);
					break;
				case "timestamp":
					self.idGenerator.init(msg.c);
				default:
			}
		});
	}

	MilkCocoa.prototype.dataStore = function(path) {
		return new DataStore(this, path);
	}

	MilkCocoa.prototype.auth = function(provider, cb) {
		var self = this;
		MilkCocoaUtil.AuthUtil.open_dialog(this.app_id, provider, function(token) {
			do_ajax("POST", self.host+"/auth", {
				cmd : "loginwithtoken",
				appid : self.app_id,
				token : token
			}, function(sid_data) {
				self.user(function(err, user){
					console.log(err, user);
					user.sid = sid_data.sid;
					cb(err, user);
				});
			});
		});
	}

	MilkCocoa.prototype.search = function(query, cb) {
		var self = this;
        var hasID = query.hasOwnProperty("id");
        var hasName = query.hasOwnProperty("name");
        var onlyID = (hasID && !hasName);
        var onlyName = (!hasID && hasName);
        if( onlyID ){
            var id = query.id;
            var re = new RegExp(/\w/);
            var isValidID = id.match(re);
            if(isValidID){
        		this.search_id(this.app_id, query.id, function(err, users) {
                    cb(err, users);
                });
            } else {
                cb(3, null);
            }
        } else if ( onlyName ) {
            var name = query.name;
            var re = new RegExp(/\w/);
            var isValidName = name.match(re);
            if(isValidName){
        		this.search_name(this.app_id, query.name, function(err, users) {
                    cb(err, users);
                });
            } else {
                cb(4, null);
            }
        } else if ( hasID && hasName ) {
            cb(1, null);
        } else if ( !hasID && !hasName ) {
            cb(2, null);
        }
	}

    MilkCocoa.prototype.search_id = function(app_id, user_id, cb){
		MilkCocoaUtil.AuthUtil.do_ajax("GET", "/auth/"+ app_id+"/id/"+user_id+"/search", {}, function(data) {
			cb(data.err, data.content);
		});
    }

    MilkCocoa.prototype.search_name = function(app_id, user_name, cb){
		MilkCocoaUtil.AuthUtil.do_ajax("GET", "/auth/"+app_id+"/name/"+user_name+"/search", {}, function(data) {
			cb(data.err, data.content);
		});
    }

	MilkCocoa.prototype.user = function(cb) {
		this.transporter.send_operation_request("user", "me", function(obj){
			var data = obj.c;
			if ( data.condition == "loggedin" ) {
				cb(null, {id: data.accountid});
			} else {
				cb(1, null);
			}
		});
	}

	MilkCocoa.prototype.getCurrentUser = function(cb) {
		this.user(cb);
	}

	MilkCocoa.prototype.logout = function(cb) {
		var self = this;
		do_ajax("POST", self.host + "/auth", {
			cmd : "logout",
			appid : self.app_id
		}, function(data) {
			reconnect(self, cb);
		});
	}

	function reconnect(self, cb) {
		self.transporter.disconnect();
		self.transporter = new MilkCocoaUtil.Transporter(self.host, {
			client : "MilkCocoa",
 			app_id : self.app_id
		}, cb);
	}

	function DataStore(milkcocoa, path) {
		this.milkcocoa = milkcocoa;
		this.transporter = milkcocoa.transporter;
		this.idGenerator = milkcocoa.idGenerator;
		this.path = path;
	}

	DataStore.prototype.push = function(params) {
		var id = this.idGenerator.getNextId();
		this.transporter.send_operation_request("push", {path : this.path, id : id, params : JSON.stringify(params)});
	}

	DataStore.prototype.set = function(id, params) {
		this.transporter.send_operation_request("set", {path : this.path, id : id, params : JSON.stringify(params)});
	}

	DataStore.prototype.send = function(params) {
		this.transporter.send_operation_request("send", {path : this.path, params : JSON.stringify(params)});
	}

	DataStore.prototype.get = function(id) {
		this.transporter.send_operation_request("get", {path : this.path, id : id});
	}

	DataStore.prototype.get_many = function(ids, cb) {
		this.transporter.send_operation_request("get_many", {path : this.path, ids : ids}, function(e) {
			cb(e.c.d.map(function(d2) {
				var value = JSON.parse(d2.value);
				value.id = d2.id;
				return value;
			}));
		});
	}

	DataStore.prototype.query = function() {
		return new Query(this.transporter, this.path);
	}

	DataStore.prototype.exec_query = function(skip, limit, sort) {
		this.transporter.send_operation_request("query", {path : this.path, skip : skip, limit : limit, sort : sort});
	}

	DataStore.prototype.remove = function(id) {
		this.transporter.send_operation_request("remove", {path : this.path, id : id});
	}

	DataStore.prototype.on = function(event, cb) {
		this.milkcocoa.listeners.add(event, this.path, cb);
		this.transporter.send_operation_request("on", {path : this.path, event : event});
	}

	DataStore.prototype.off = function(event) {
		this.milkcocoa.listeners.del(event, this.path);
		this.transporter.send_operation_request("off", {path : this.path, event : event});
	}

	DataStore.prototype.child = function(child_path) {
		return new DataStore(this.milkcocoa, this.path + "/" + child_path);
	}

	function Query(transporter, path) {
		this.transporter = transporter;
		this.path = path;
		this._skip = 0;
		this._limit = 10;
		this._sort = "DESC";
	}

	Query.prototype.skip = function(_skip) {
		this._skip = _skip;
		return this;
	}

	Query.prototype.limit = function(_limit) {
		this._limit = _limit;
		return this;
	}

	Query.prototype.sort = function(_sort) {
		this._sort = _sort;
		return this;
	}

	Query.prototype.done = function(cb) {
		this.transporter.send_operation_request("query", {path : this.path, skip : this._skip, limit : this._limit, sort : this._sort}, function(e) {
			if(e.e == "error") {
				cb(e.c, []);
				return;
			}
			cb(null, e.c.d.map(function(d2) {
				var value = JSON.parse(d2.value);
				value.id = d2.id;
				return value;
			}));
		});
	}

	Query.prototype.count = function(cb) {
		this.transporter.send_operation_request("count", {path : this.path, skip : this._skip, limit : this._limit, sort : this._sort}, function(e) {
			if(e.e == "error") {
				cb(e.c);
				return;
			}
			cb(e.c);
		});
	}

	Query.prototype.start = function(cb) {
		this.transporter.send_operation_request("streaming", {path : this.path, skip : this._skip, limit : this._limit, sort : this._sort}, function(e) {
			if(e.e == "error") {
				cb(e.c);
				return;
			}
			cb(null,e.c.d.map(function(d2) {
				var value = JSON.parse(d2.value);
				value.id = d2.id;
				return value;
			}));
		});
	}

	function do_ajax(method, url, params, onsuccess) {
		var xhr = createCORSRequest(method , url);
    	xhr.withCredentials = true;
    	xhr.onload = function() {
    		onsuccess(xhr.responseText);
    		//onsuccess(JSON.parse(xhr.responseText));
    	}
    	var params_str = querystring(params);
    	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    	xhr.send(params_str);
    }

    function createCORSRequest(method, url) {
	    var xhr = new XMLHttpRequest();
	    if ("withCredentials" in xhr) {
	        xhr.open(method, url, true);
	    } else if (typeof XDomainRequest != "undefined") {
	        xhr = new XDomainRequest();
	        xhr.open(method, url);
	    } else {
	        xhr = null;
	    }
	    return xhr;
	}

	function querystring(params) {
		var params_array = []
		for(var key in params) {
			params_array.push(key + "=" + encodeURIComponent(params[key]));
		}
		return params_array.join("&");
	}

	global.MilkCocoa = MilkCocoa;
	global.querystring = querystring;
	global.myconsole = myconsole;

}(window));
