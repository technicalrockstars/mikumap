(function(global){

	var shuffle_table = 'ybfghijam6cpqdrw71nx34eo5suz0t9vkl28';
	function IdGenerator() {
		this.client_timestamp = new Date().getTime();
		this.prev_id = 0;
	}
	IdGenerator.prototype = {
		init : function(ts) {
			this.timestamp = ts;
		},
		getHeader : function(t) {
			var diff = new Date().getTime() - this.client_timestamp;
			return (t + diff).toString(36);
		},
		getHash : function(time) {
			var h = '';
			var t = time;
			while(t > 0) {
				h += shuffle_table[t % 36];
				t = t/36;
				t = Math.floor(t);
			}
			return h;
		},
		getNextId : function() {
			return this.getHeader(this.timestamp) + add_random(7);
			function add_random(n) {
				if(n <= 1)
					return shuffle_table[Math.floor(Math.random() * 36)];
				else
					return shuffle_table[Math.floor(Math.random() * 36)] + add_random(n - 1);
			}
		}
	}

	if(!global.hasOwnProperty("MilkCocoaUtil"))
		global.MilkCocoaUtil = {};
	global.MilkCocoaUtil.IdGenerator = IdGenerator;

}(window));
