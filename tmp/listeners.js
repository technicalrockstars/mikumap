(function(global){

	function Listeners() {
		this._listeners = {
			"push" : {},
			"set" : {},
			"remove" : {},
			"send" : {}
		};
	}

	Listeners.prototype.add = function(event, path, cb) {
		var path = MilkCocoaUtil.OperationEncoder.encode_erlang_path(path);
		if(!this._listeners[event][path])
			this._listeners[event][path] = [];
		this._listeners[event][path].push(cb);
	}

	Listeners.prototype.del = function(event, path) {
		var path = MilkCocoaUtil.OperationEncoder.encode_erlang_path(path);
		this._listeners[event][path] = [];
	}

	Listeners.prototype.fire = function(event, path, arg) {
		var path = MilkCocoaUtil.OperationEncoder.encode_erlang_path(path);
		this._listeners[event][path].forEach(function(l) {
			l(arg);
		});
	}

	if(!global.hasOwnProperty("MilkCocoaUtil"))
		global.MilkCocoaUtil = {};
	global.MilkCocoaUtil.Listeners = Listeners;

}(window));
