(function(global){

	var MilkCocoaAuthUtil = {
		auth_host : "https://v2-stage-auth.mlkcca.com",
		open_dialog : function(app_id, provider, onTokenGetted) {
			var api_server = this.auth_host;
			var url = api_server + "/auth/"+app_id+"/"+provider+"/dialog?mlk_redirect_uri=" + encodeURIComponent(window.location.href);
			transport_window(url);
			function transport_redirect(url) {
				global.location = url;
			}
			function transport_window(url) {
				var window_features = {
					'menubar'    : 1,
					'location'   : 0,
					'resizable'  : 0,
					'scrollbars' : 1,
					'status'     : 0,
					'dialog'     : 1,
					'width'      : 300,
					'height'     : 150
				};
				var child = window.open(url, "Login", window_features);
				addListener(child, 'unload', function() {

				});
				addListener(window, 'message', function(e) {
					//get Json Web Token
					if(e.data != "__ready__")
						onTokenGetted(e.data);
				});
			}
			function addListener(w, event, cb) {
				if (w['attachEvent']) w['attachEvent']('on' + event, cb);
				else if (w['addEventListener']) w['addEventListener'](event, cb, false);
			}
		},
		do_ajax : function(method, path, params, onsuccess) {
	        var url = this.auth_host + path;
			var xhr = createCORSRequest(method , url);
	    	xhr.withCredentials = false;
	    	xhr.onload = function() {
	    		onsuccess(JSON.parse(xhr.responseText));
	    	}
	    	var params_str = querystring(params);
	    	xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	    	xhr.send(params_str);
		    function createCORSRequest(method, url) {
			    var xhr = new XMLHttpRequest();
			    if ("withCredentials" in xhr) {
			        xhr.open(method, url, true);
			    } else if (typeof XDomainRequest != "undefined") {
			        xhr = new XDomainRequest();
			        xhr.open(method, url);
			    } else {
			        xhr = null;
			    }
			    return xhr;
			}
		}
	}

	if(!global.hasOwnProperty("MilkCocoaUtil"))
		global.MilkCocoaUtil = {};
	global.MilkCocoaUtil.AuthUtil = MilkCocoaAuthUtil;

}(window));
