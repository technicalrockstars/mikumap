(function(global){
    function SearchUser() {}

    SearchUser.search = function(e, milkcocoa, ds_chat){
        $('.userlist').remove();
        var $userlist = $('<ul class="userlist"></ul>');
        setTimeout(function(){
            if(e.target.value.length > 0){
                milkcocoa.search({name:Util.escapeHTML(e.target.value)},function(err, users){
                    if(err == null){
                        if(users.length > 0){
                            users.map(function(user){
                                var id = "item-"+Util.serialized_name(user.name);
                                $userlist.append('<li class="listitem" id="'+id+'">'+user.name+'</li>');
                                $("#"+id).click(function(e){
                                    ds_chat.child("allow").set(Util.serialized_name(user.name), {name:user.name},function(e){
                                        DataTable.add_member(user.name, ds_chat);
                                    });
                                });
                                return true;
                            });
                            $userlist.children().click(function(e){
                                var name = e.target.innerText;
                                DataTable.add_member(name, ds_chat);
                                ds_chat.child("allow").set(Util.serialized_name(name), {name:name});
                                $("#result").empty();
                            });
                            $("#result").append($userlist);
                        }
                    }
                });
            }
        }, 0);
    }

    global.SearchUser = SearchUser;

}(window));
