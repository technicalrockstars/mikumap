(function(global){
    function Util() {}

    Util.escapeHTML = function(val) {
        return $('<div />').text(val).html();
    }

    Util.serialized_name = function(name) {
        if(name.length > 0){
            var space_name_array = name.split(" ");
            if(space_name_array.length > 0) return space_name_array.join("");
            else return "";
        } else {
            return "";
        }
    }

    global.Util = Util;
}(window));
