(function() {
    $.getJSON("http://"+location.host+"/conf/data.json", function(data) {
        var milkcocoa = new MilkCocoa("https://" + data.app_id + ".mlkcca.com");

        milkcocoa.getCurrentUser(function(err, user){
          console.log(err, user);
          var isLoggedIn = (user != null);
          if (isLoggedIn) {
            $("#index--menu").append('<li class="header--rooms"><a href="rooms.html"><i class="btn fa fa-info"></i>my rooms</a></li>');
            $("#index--menu").append('<li class="header--logout" id="logout"><a><i class="btn fa fa-info"></i>logout</a></li>');
            $(".index--room-message").text("マインドマップチャットを使おう");
            $("#logout").on("click", function(e){
              milkcocoa.logout(function(){
                location.reload();
              });
            });


            $(".index--box").css("display","block");

            var room_textbox = $('.index--input--room');
            room_textbox.on("keydown", function(e){
                setTimeout(function(){
                    var url = "/mikumap.html#" + Util.escapeHTML(e.target.value);
                    if (e.which === 13) location.href = url;
                }, 0);
            });

            var search_id_textbox = $('.index--input--search-id');
            search_id_textbox.on("keydown", function(e){
                setTimeout(function(){
                    milkcocoa.search({id:Util.escapeHTML(e.target.value)}, function(err, users){
                        if(err == null){
                            users.map(function(user){
                                $(".userlist").append('<p>'+user.id+'</p>');
                                return true;
                            });
                        } else {
                            console.error("no data");
                        }
                    });
                }, 0);
            });

            var search_name_textbox = $('.index--input--search-name');
            search_name_textbox.on("keydown", function(e){
                setTimeout(function(){
                    milkcocoa.search({name:Util.escapeHTML(e.target.value)}, function(err, users){
                        if(err == null){
                            users.map(function(user){
                                $(".userlist").append('<p>'+user.name+'</p>');
                                return true;
                            });
                        } else {
                            console.error("no data");
                        }
                    });
                }, 0);
            });

          } else {
            $("#index--menu").append('<li class="header--fbauth" id="fb_auth"><a><i class="btn fa fa-info"></i>fb-login</a></li>');
            $("#index--menu").append('<li class="header--twauth" id="tw_auth"><a><i class="btn fa fa-info"></i>tw-login</a></li>');

            $("#fb_auth").on("click", function(e){
              milkcocoa.auth("facebook", function(err, data){
                console.log(err, data);
                switch(err){
                  case null:
                    location.reload();
                    break;
                  case 1:
                    console.log("oauth failed");
                    break;
                }
              });
            });

            $("#tw_auth").on("click", function(e){
              milkcocoa.auth("twitter", function(err, data){
                console.log(err, data);
                switch(err){
                  case null:
                    location.reload();
                    break;
                  case 1:
                    console.log("oauth failed");
                    break;
                }
              });
            });

          }
        });
    });
}());
