(function(global){
  /* roomの変更の監視 */
    $(window).on('hashchange', function(){
      location.reload();
    });

    $.getJSON("http://"+location.host+"/conf/data.json", function(data) {
        var milkcocoa = new MilkCocoa("https://" + data.app_id + ".mlkcca.com");
        milkcocoa.dataStore("aあb").on('push', function(e){});
        milkcocoa.dataStore("aあb").push({});
        var ROOT;
        var DICTIONARY = {};
        var room_id = decodeURI(location.hash.substr(1));
        var ds_rooms = milkcocoa.dataStore('rooms');

        milkcocoa.getCurrentUser(function(err, user){
            console.log(err, user);

            /* define */
            var isLoggedIn = (err == null);
            if (isLoggedIn){
                var owner_id = user.id;
                var chatstore = (!room_id) ? 'chats' : 'chats/' + owner_id + '/' + room_id;
                var ds = milkcocoa.dataStore(chatstore);

                $(document).ready(function() {

                    ds_rooms.set(room_id, { owner_id: owner_id, updated_date: new Date() });

                    $('body').mindmap();

                    if (room_id) addNewNode('asRoot','#'+room_id,'');
                    else addNewNode('asRoot','Ｙ(๑°口°๑)','');

                    getDataAndBuildFrom(ds);
                });

                $(document).on("keydown", function(e){
                    if(e.which === 13) $('div.active').trigger('click');
                });

                ds.on("push", function(e){
                    var parent_node = DICTIONARY[e.value.parent_path];
                    addNewNode( parent_node, escapeHTML(e.value.label), escapeHTML(e.value.name) )
                });
            } else {
              alert("login!");
              location.href = "http://"+location.host;
            }

            function getDataAndBuildFrom(ds){
                ds.query({}).done(function(err, data){
                    if(err == null){
                        data.sort(function(a,b){
                            var max = a.node_path.length;
                            var min = b.node_path.length;
                            if(min < max)return 1;
                            else if (min == max)return 0;
                            else return -1
                        });
                        data.forEach(function(elem, index, array){
                            addNewNode(DICTIONARY[elem.parent_path], elem.label, elem.name);
                        });

                        /* レンダリング終了後childrenにする */
                        ROOT.children.forEach(function(node){
                          node.el.addClass('children');
                        });
                    } else {
                        console.error("query error");
                    }
                });
            }

            function addNewNode(parentNode, label, name){
                var label = escapeHTML(label);
                var name = escapeHTML(name);

                var returnNode;
                var onclick = function(node) {
                    if(node === CURRENT_NODE){
                        /* show modal */
                        $('#modal-dialog').modal('show');
                        var wLimit = 40;

                        /* focus textarea */
                        /* バリデーションメッセージを隠す */
                        $('#modal-dialog').on('shown.bs.modal', function () {
                            $("#label").focus();
                            $("p.count").hide();
                            $("p.init-count").hide();
                            $("p.modal-alert").hide();

                            var wLength = escapeHTML($("#label").val()).length;
                            var wRemain = wLimit - wLength;
                            $("div.modal-footer").prepend('<p class="init-count">' + wRemain + '</p>');
                        });

                        /* when keyup length limit */
                        $("#label").off("keydown keyup").on("keydown keyup", function(e){
                            if ( $("p.init-count") ) $("p.init-count").hide();
                            if ( $("p.count") ) $("p.count").hide();

                            var wLength = escapeHTML($("#label").val()).length;
                            var wRemain = wLimit - wLength;
                            if ( wRemain > 0 ){
                                $("div.modal-footer").prepend('<p class="count">' + wRemain + '</p>');
                            } else {
                                $("div.modal-footer").prepend('<p class="count count-zero">' + wRemain + '</p>');
                            }
                        });

                        $("#label").off("keypress").on("keypress", function(e){
                            // metakeyが死んでる
                            /* submit */
                            if((e.shiftKey||e.metaKey||e.ctrlKey)&&e.which===13) $("#submit").trigger('click');
                            e.stopPropagation(); //親への伝播防止
                        });

                        /* click submit */
                        $("#submit").off("click").on("click", function(){
                            submit(node);
                        });
                    }else{
                        $(node.obj.activeNode.content).each(function() {
                            this.hide();
                        });
                    }
                };
                var onContextMenu = function(node){};
                //root Node
                if(parentNode == null || parentNode == 'root' || parentNode == 'ROOT' || parentNode == 'asRoot'){
                    if(!ROOT){
                        ROOT = $('body').addRootNode('noname', {
                            name:name,
                            label:label,
                            path:'root',
                            onclick:onclick,
                            onContextMenu:onContextMenu
                        });
                    }
                    returnNode = ROOT
                }
                //normal node
                else{
                    var node = $('body').addNode(parentNode, 'noname', {
                        label:label,
                        name:name,
                        path:parentNode.opts.path + '/' + label,
                        onclick:onclick,
                        onContextMenu:onContextMenu
                    });
                    returnNode = node
                }
                DICTIONARY[returnNode.opts.path] = returnNode;
                return returnNode;
            }

            function submit(node){
                var label = escapeHTML( $("textarea#label").val() );
                var name = escapeHTML( $("textarea#name").val() );
                var parent_path = node.opts.path;
                var node_path = parent_path + "/" + label;
                var ds = milkcocoa.dataStore(chatstore);
                if(!name){ name = "" }
                if(label && !DICTIONARY[node_path]){
                    ds.push({
                        "label" : label,
                        "name" : name,
                        "node_path" : node_path,
                        "parent_path" : parent_path
                    });
                    $("textarea#label").val('');
                    $('#modal-dialog').modal('hide');
                    var ds_rooms = milkcocoa.dataStore('rooms');
                    ds_rooms.set(room_id, { owner_id: owner_id, "updated_date" : new Date() });
                } else {
                    $("div.modal-footer").append("<p class='modal-alert pull-left'>コメントは空白と重複がないようにしてください</p>");
                }
                return false;
            };

        });

        function getParentPath(node){
            if (node.parent) {
                return node.parent.opts.path;
            } else {
                //case root
                return '';
            }
        }

        function clearDataStore(ds){
            ds.query({}).done(function(err, data){
                if(err == null){
                    data.forEach(function(elem){
                        // console.log('delete data id = ' + elem.id);
                        ds.remove(elem.id);
                    });
                } else {
                    console.error("no data");
                }
            });
        }

        function escapeHTML(val) {
            return $('<div />').text(val).html();
        };

        var check_sp = (ua('iPhone') > 0 && ua('iPad') == -1) || ua('iPod') > 0 || ua('Android') > 0;
        if (check_sp) $(".modal-dialog").addClass('modal-sm');
        function ua(user_agent){
            return navigator.userAgent.indexOf(user_agent);
        }
    });
}(window));
