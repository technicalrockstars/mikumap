(function(){
    $.getJSON("http://"+location.host+"/conf/data.json", function(data) {
        var milkcocoa = new MilkCocoa("https://" + data.app_id + ".mlkcca.com:443");
        var room_id = location.hash.substr(1, location.hash.length-1);

        milkcocoa.getCurrentUser(function(err, user){
            var owner_id = user.id;
            var ds_chat = milkcocoa.dataStore('chats/'+owner_id+"/"+room_id);
            var allowsTable = DataTable.allowsTable(ds_chat);
            var isLoggedIn = (err == null);

            $(function() {
                $("body").prepend("<h1>"+room_id+"</h1>");
                if (isLoggedIn) allowsTable.render();
                else location.href = data.root.dev;
            });

            ds_chat.child('allow').on("set", function(new_member){
                allowsTable.add_member(new_member.name, ds_chat);
            });

            $('#search_area').on("keydown", function(e){
                SearchUser.search(e, milkcocoa, ds_chat);
            });
        });

        function gen_id() {
            return "id" + String(Math.random()).substr(2);
        }
    });
}());
