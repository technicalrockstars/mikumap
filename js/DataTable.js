(function(global){
    function DataTable() {}

    DataTable.allowsTable = function(ds_chat) {
        return new AllowsTable(ds_chat);
    }
    DataTable.roomsTable = function(milkcocoa, ds_rooms, room_map) {
        return new RoomsTable(milkcocoa, ds_rooms, room_map);
    }

    function AllowsTable(ds_chat) {
        this.ds_chat = ds_chat;
    }
    AllowsTable.prototype.render = function(){
        var self = this;
        self.ds_chat.child("allow").query({}).done(function(err, allows){
            if(err == null){
                if(allows == "permission denied") throw 'error that ds_chat.child("allow").query has denied';
                if(allows == null) throw 'error that ds_chat.child("allow").query has null';
                allows.forEach(function(allowed_member){
                    self.add_member(allowed_member.name, self.ds_chat);
                    $('#allows').dataTable();
                });
            } else {
                throw("no data");
            }
        });
    }
    AllowsTable.prototype.add_member = function(name, ds_chat) {
        var self = this;
        $("#records").append(self.gen_record_dom(name));
        var listid = Util.serialized_name(name);
        var deleteid = listid+"-delete";
        $('#'+deleteid).click(function(e){
            $("#"+listid).remove();
            ds_chat.child("allow").remove(Util.serialized_name(name));
        });
    }
    AllowsTable.prototype.gen_record_dom = function(name, ncomment, latest_comment){
        var listid = Util.serialized_name(name);
        return "<tr id='"+listid+"'><th>" + name + "</th><th><button id='"+listid+"-delete'>delete</button></th></tr>";
    }

    function RoomsTable(milkcocoa, ds_rooms, room_map) {
        this.milkcocoa = milkcocoa;
        this.ds_rooms = ds_rooms;
        this.room_map = room_map;
    }
    RoomsTable.prototype.renderMyRoom = function(owner_id){
        var self = this;
        self.ds_rooms.query({owner_id: owner_id}).done(function(err, rooms){
            if(err == null){
                if(rooms == "permission denied") throw 'error that ds_rooms.query has denied';
                $("body").prepend("rooms:" + rooms.length);
                rooms.forEach(function(room){
                    var ds_chat = self.milkcocoa.dataStore('chats/'+owner_id+"/"+room.id);
                    ds_chat.query({}).sort('desc').limit(100).done(function(err, comments){
                        ds_chat.child("allow").query({}).done(function(err, allows){
                            if(err == null){
                                var new_comment = (comments[0]) ? comments[0].label : 'none';
                                var element_id = self.gen_id();
                                self.room_map[room.id] = element_id;
                                self.add_room(element_id, room.id, comments.length, new_comment, allows);
                                $('#rooms').dataTable();
                            } else {
                                console.error("no data");
                            }
                        });
                    });
                });
            } else {
                console.error("no data");
            }
        });
    }
    RoomsTable.prototype.renderInvitedRoom = function(owner_id){
        var self = this;
        $("#invitedRooms");
    }

    RoomsTable.prototype.add_room = function(id, room_id, ncomment, latest_comment, allows) {
        $("#records").append(this.gen_record_dom(id, room_id, ncomment, latest_comment, allows));
    }
    RoomsTable.prototype.update_room = function(id, room_id, ncomment, latest_comment, allows) {
        $("#" + id).html(this.gen_record_dom(id, room_id, ncomment, latest_comment, allows));
    }
    RoomsTable.prototype.gen_record_dom = function(id, room_id, ncomment, latest_comment, allows){
        return "<tr id='"+id+"'><th><a class='rooms--roomlist' href='mikumap.html#"+ decodeURI(room_id) + "'>" + decodeURI(room_id) + "</a></th><th>" + ncomment + "</th><th>" + latest_comment + "</th><th><a href='allows.html#"+room_id+"'>"+allows.length+"</a></th></tr>";
    }
    RoomsTable.prototype.gen_id = function() {
        return "id" + String(Math.random()).substr(2);
    }


    global.DataTable = DataTable;
}(window));
