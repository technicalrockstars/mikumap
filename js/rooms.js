(function(){
    $.getJSON("http://"+location.host+"/conf/data.json", function(data) {
        var milkcocoa = new MilkCocoa("https://" + data.app_id + ".mlkcca.com:443");
        var room_map = {};
        var ds_rooms = milkcocoa.dataStore('rooms');
        var roomsTable = DataTable.roomsTable(milkcocoa, ds_rooms, room_map);

        milkcocoa.getCurrentUser(function(err, user){
            var isLoggedIn = (err == null);
            $(function() {
                if (isLoggedIn) {
                    roomsTable.renderMyRoom(user.id);
                    roomsTable.renderInvitedRoom(user.id);
                } else location.href = data.root.dev;
            });
        });

        ds_rooms.on("set", function(room){
            ds_chats.child(room.id).query({}).sort('desc').limit(100).done(function(err, comments){
                var new_comment = (comments[0]) ? comments[0].label : "-";
                if(roomsTable.room_map.hasOwnProperty(room.id)) {
                    ds_chats.child("allow").query(function(err, allows){
                        roomsTable.update_room(roomsTable.room_map[room.id], room.id, comments.length, new_comment, allows);
                    });
                }else{
                    var element_id = roomsTable.gen_id();
                    roomsTable.room_map[room.id] = element_id;
                    roomsTable.add_room(element_id, room.id, comments.length, new_comment, []);
                }
            });
        });
    });
}());
